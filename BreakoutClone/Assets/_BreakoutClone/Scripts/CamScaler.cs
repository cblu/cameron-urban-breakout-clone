using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CamScaler : MonoBehaviour
{
    public float cameraWidth = 10;
    public Transform topWall;

    private void Update()
    {
        Camera cam = Camera.main;
        float aspect = Mathf.Clamp(Camera.main.aspect, 0.1f, 0.7f);
        cam.orthographicSize = (cameraWidth/2) / aspect;

        // Move camera up so that the bottom aligns to y = 0
        cam.transform.position = -Vector3.forward * 10 + Vector3.up * Camera.main.orthographicSize;

        // Move top wall to align with top of the camera
        topWall.position = (Vector2)cam.ViewportToWorldPoint(new Vector2(0.5f, 1));
    }
}
