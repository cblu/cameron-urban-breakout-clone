using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelpler
{
    /// <summary>
    /// Play an <see cref="AudioClip"/>.
    /// </summary>
    /// <param name="audioClip">The clip to play.</param>
    public static void PlayAudio(AudioClip audioClip, GameObject gameObject)
    {
        if (audioClip == null)
            return;

        AudioSource m_EffectsAudioSource = gameObject.GetComponent<AudioSource>();
        if (m_EffectsAudioSource == null)
            m_EffectsAudioSource = CreateEffectsAudioSource(gameObject);

        m_EffectsAudioSource.pitch = 1;
        m_EffectsAudioSource.PlayOneShot(audioClip);
    }

    public static void PlayAudio(AudioClip audioClip, GameObject gameObject, float volume)
    {
        if (audioClip == null)
            return;

        AudioSource m_EffectsAudioSource = gameObject.GetComponent<AudioSource>();
        if (m_EffectsAudioSource == null)
            m_EffectsAudioSource = CreateEffectsAudioSource(gameObject);

        m_EffectsAudioSource.pitch = 1;
        m_EffectsAudioSource.PlayOneShot(audioClip, volume);
    }

    public static void PlayAudio(AudioClip audioClip, GameObject gameObject, float volume, float pitch, float spacialBlend = 0)
    {
        if (audioClip == null)
            return;

        AudioSource m_EffectsAudioSource = gameObject.GetComponent<AudioSource>();
        if (m_EffectsAudioSource == null)
            m_EffectsAudioSource = CreateEffectsAudioSource(gameObject);

        m_EffectsAudioSource.pitch = pitch;
        m_EffectsAudioSource.spatialBlend = spacialBlend;
        m_EffectsAudioSource.clip = audioClip;
        m_EffectsAudioSource.volume = volume;
        m_EffectsAudioSource.Play();
    }

    static AudioSource CreateEffectsAudioSource(GameObject gameObject)
    {
        AudioSource m_EffectsAudioSource = gameObject.AddComponent<AudioSource>();
        m_EffectsAudioSource.loop = false;
        m_EffectsAudioSource.playOnAwake = false;

        return m_EffectsAudioSource;
    }
}
