using Mirror;
using UnityEngine;

public class Ball : NetworkBehaviour
{
    public float speed = 30;
    public AudioClip[] wallBounceSounds;
    public AudioClip[] paddleBounceSounds;

    [SyncVar]
    private bool launched = false;
    [HideInInspector, SyncVar]
    public GameObject owner;

    private Rigidbody2D rigidbody;
    public SpriteRenderer ballRenderer;
    public SpriteRenderer glowRenderer;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Launch()
    {
        if (hasAuthority)
        {
            float launchAngle = 45 + Random.Range(0, 90);
            rigidbody.velocity = new Vector2(Mathf.Cos(launchAngle * Mathf.Deg2Rad), Mathf.Sin(launchAngle * Mathf.Deg2Rad)) * speed;
            launched = true;

            // Send launch command to server to release ball from paddle
            CmdLaunch();
        }
    }

    [Command]
    public void CmdLaunch()
    {
        launched = true;
    }

    [Command]
    public void CmdReset()
    {
        launched = false;
    }

    // Used to find how far along the paddle the ball was hit
    private float HitFactor(Vector2 ballPos, Vector2 racketPos, float racketWidth)
    {
        // ||  -1 <- at the left of the paddle
        // ||
        // ||  0 <- at the middle of the paddle
        // ||
        // ||  1 <- at the right of the paddle
        return (ballPos.x - racketPos.x) / racketWidth;
    }

    private void Update()
    {
        // If the ball hasn't been launched then move it with the possessing player's paddle
        if (!launched)
        {
            if (owner)
                transform.position = owner.GetComponent<Player>().ballAnchor.position;
        }
        else
        {
            // Prevent the ball from moving too fast
            rigidbody.velocity += (rigidbody.velocity.normalized * speed - rigidbody.velocity) * Time.deltaTime;

            // Ensure that the velocity never reaches y = 0
            if (Mathf.Abs(rigidbody.velocity.y) <= 0.01f)
                rigidbody.velocity = (rigidbody.velocity + Vector2.down).normalized * speed;
        }
    }

    public void UpdateColor(Color col)
    {
        float glowAlpha = glowRenderer.color.a;
        Color newCol = Color.Lerp(glowRenderer.color, col, 0.8f);
        glowRenderer.color = new Color(newCol.r, newCol.g, newCol.b, glowAlpha);
        glowRenderer.GetComponent<LightFader>().initialLightColor = glowRenderer.color;
        ballRenderer.color = Color.Lerp(ballRenderer.color, col, 0.5f);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Player player = col.transform.GetComponent<Player>();
        // Did we hit a racket? then we need to calculate the hit factor
        if (player)
        {
            // Calculate x direction via hit Factor
            float x = HitFactor(transform.position,
                                col.transform.position,
                                player.width * col.transform.localScale.x);

            // Calculate y direction via opposite collision
            float y = col.relativeVelocity.y > 0 ? 1 : -1;

            // Calculate direction, make length=1 via .normalized
            Vector2 dir = new Vector2(x, y).normalized;

            rigidbody.velocity = dir * speed;

            AudioHelpler.PlayAudio(paddleBounceSounds[Random.Range(0, paddleBounceSounds.Length)], gameObject, Random.Range(0.6f, 0.7f), Random.Range(0.9f, 1.1f));

            player.PaddleHit();
        }

        // If we hit the brick then break the brick
        Brick brick = col.transform.GetComponent<Brick>();
        if (brick && hasAuthority)
        {
            brick.Break();

            GameManager.Instance.AddPoints(100);
        }

        AudioHelpler.PlayAudio(wallBounceSounds[Random.Range(0, wallBounceSounds.Length)], gameObject, Random.Range(0.5f, 0.7f), Random.Range(0.9f, 1.1f));

        GameManager.Instance.TriggerLightFlash();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // On entering the redzone, reset the ball to the paddle of the last player that hit it
        if (other.CompareTag("RedZone"))
        {
            Debug.Log("Ball entered red zone");

            CmdReset();

            GameManager.Instance.PlayerLost();
        }
    }
}
