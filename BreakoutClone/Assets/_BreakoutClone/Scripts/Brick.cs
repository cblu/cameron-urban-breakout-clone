using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Brick : NetworkBehaviour
{
    public Sprite[] sprites;
    public Color[] colours;
    public AudioClip[] breakSounds;
    public ParticleSystem particles;
    public SpriteRenderer patternSprite;

    [SyncVar(hook = nameof(UpdateRow))]
    private int _row = -1;
    public int row { get => _row; set => _row = value; }

    [SyncVar(hook = nameof(UpdateWidth))]
    private float _width = 2.5f;
    public float width { get => _width; set => _width = value; }


    // Cached components
    SpriteRenderer renderer;
    BoxCollider2D collider;


    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        // Select a random brick pattern
        patternSprite.sprite = sprites[Random.Range(0, sprites.Length)];
        
        // Set row to 0 if it hasn't been set by the gamemanager
        if (_row == -1)
            _row = 0;
    }

    public void UpdateWidth(float oldWidth, float width)
    {
        // Update sprites and collider
        collider.size = new Vector2(_width, collider.size.y);
        renderer.size = new Vector2(_width, renderer.size.y);
        patternSprite.size = new Vector2(_width, patternSprite.size.y);

        // Update particle system shape
        var pShape = particles.shape;
        pShape.scale = new Vector2(_width, pShape.scale.y);
    }

    public void UpdateRow(int oldRow, int row)
    {
        patternSprite.color = colours[(int)Mathf.Repeat(_row, colours.Length)];
        var pMain = particles.main;
        pMain.startColor = patternSprite.color;
    }

    public void Break()
    {
        OnBreak();
        CmdBreak();
    }

    [Command(requiresAuthority = false)]
    public void CmdBreak()
    {
        RpcBreak();
    }

    [ClientRpc]
    private void RpcBreak()
    {
        OnBreak();
    }

    private async void OnBreak()
    {
        // Play break animation and sound
        particles.Play();
        particles.transform.SetParent(null, true);
        AudioHelpler.PlayAudio(breakSounds[Random.Range(0, breakSounds.Length)], particles.gameObject, Random.Range(0.9f, 1.1f), Random.Range(0.9f, 1.1f));

        await Task.Yield();
        Destroy(gameObject);
    }
}
