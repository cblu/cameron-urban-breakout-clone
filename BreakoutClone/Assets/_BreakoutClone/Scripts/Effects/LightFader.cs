using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LightFader : MonoBehaviour
{
    SpriteRenderer renderer;

    [HideInInspector]
    public Color initialLightColor;

    private void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        initialLightColor = renderer.color;

        GameManager.Instance.FlashLights.AddListener(OnFlashLights);
    }

    private void Update()
    {
        renderer.color = Color.Lerp(renderer.color, initialLightColor, Time.deltaTime / 0.15f);
    }

    private void OnFlashLights()
    {
        renderer.color = new Color(Color.white.r, Color.white.g, Color.white.b, initialLightColor.a *0.7f);
    }
}
