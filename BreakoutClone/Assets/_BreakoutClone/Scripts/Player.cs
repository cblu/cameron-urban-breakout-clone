using Mirror;
using UnityEngine;

public class Player : NetworkBehaviour
{
    public float speed = 30;
    public Transform ballAnchor;
    public GameObject ballPrefab;
    public Color[] playerColors;
    [HideInInspector]
    [SyncVar(hook = nameof(OnSetPlayerId))]
    public int playerId = -1;

    private float _width = 3;
    public float width { get => _width; set => UpdateWidth(value); }

    private GameObject ball;
    private float initialY;

    // Cached components
    private Rigidbody2D rigidbody;
    private BoxCollider2D collider;
    private SpriteRenderer renderer;
    private ParticleSystem burstParticles;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
        renderer = GetComponent<SpriteRenderer>();
        burstParticles = GetComponentInChildren<ParticleSystem>();

        initialY = transform.position.y;
    }

    private void Start()
    {
        // Set player initial width
        UpdateWidth(3);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        // Create a ball for the player and give them authority over it
        GameObject newBall = Instantiate(ballPrefab);
        NetworkServer.Spawn(newBall, connectionToClient);
        ball = newBall;
        ball.GetComponent<Ball>().owner = gameObject;
    }

    private void OnSetPlayerId(int oldId, int newId)
    {
        // If id is already set then dont set the colours twice
        if (oldId == newId)
            return;

        // When the player is assigned an id update the sprite colors appropriately
        int id = Mathf.Clamp(newId, 0, playerColors.Length - 1);
        renderer.color = Color.Lerp(renderer.color, playerColors[id], 0.3f);

        // If we dont have a reference to the ball then request one from the server
        if (!ball)
            CmdRequestBall();
        else
            OnSetBall();
    }

    [Command(requiresAuthority = false)]
    private void CmdRequestBall()
    {
        RpcSetBall(ball);
    }

    [ClientRpc]
    private void RpcSetBall(GameObject ball)
    {
        if (!this.ball)
        {
            this.ball = ball;

            OnSetBall();
        }
    }

    // When the ball is assigned to the player and the player has an Id, update the ball's color
    private void OnSetBall()
    {
        if (playerId == -1)
            return;
        Debug.Log("NetID: " + playerId);

        int id = Mathf.Clamp(playerId, 0, playerColors.Length - 1);

        if (ball)
            ball.GetComponent<Ball>().UpdateColor(playerColors[id]);
    }

    private void Update()
    {
        if (Input.GetButtonDown("LaunchBall"))
        {
            ball.GetComponent<Ball>().Launch();
        }

        // Ensure no vertical drift due to collision inaccuracies
        transform.position = new Vector2(transform.position.x, initialY);
    }

    // need to use FixedUpdate for rigidbody
    void FixedUpdate()
    {
        // only let the local player control the paddle
        if (isLocalPlayer)
        {
            Vector2 targetVel = new Vector2(Input.GetAxisRaw("Horizontal"), 0) * speed * Time.fixedDeltaTime;
            if (Vector2.Dot(targetVel, rigidbody.velocity) < 0)
                rigidbody.velocity = Vector2.zero;
            rigidbody.velocity += (targetVel - rigidbody.velocity) * Mathf.Clamp01(Time.deltaTime * 20);
        }
    }

    private void UpdateWidth(float newWidth)
    {
        collider.size = new Vector2(newWidth, 1);
        renderer.size = new Vector2(newWidth, 1);
        _width = newWidth;
    }

    public void PaddleHit()
    {
        burstParticles.Play();
    }

    private void OnDestroy()
    {
        Destroy(ball);
    }
}
