using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text scoreLabel;

    // Set up singleton
    private static UIManager _instance;

    public static UIManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
    }

    private void Start()
    {
        // Hide score on start
        scoreLabel.gameObject.SetActive(false);
    }

    public void UpdateScore(int score)
    {
        scoreLabel.gameObject.SetActive(true);
        scoreLabel.text = "SCORE: " + score;
    }
}
