using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : NetworkBehaviour
{
    public GameObject brickPrefab;
    public AudioClip loseSound;
    
    [SyncVar(hook = nameof(SetScore))]
    private float gameScore = 0;

    // Component references
    private CamScaler camScaler;

    // Events
    [HideInInspector]
    public UnityEvent FlashLights;

    // Set up singleton
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;

        // Cache component references
        camScaler = Camera.main.GetComponent<CamScaler>();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();

        // Spawn bricks on game start
        if (!camScaler) {
            Debug.Log("No camera scaler found on the main camera");
            return;
        }

        SpawnBricks();

        SetScore(0);
    }

    public void SpawnBricks()
    {
        // Define brick grid
        float topOffset = 1.5f;
        float margin = 1f;
        float padding = 0.05f;
        float brickHeight = 0.5f;
        int columns = 10;
        int rows = 5;

        // Spawn bricks
        Camera cam = Camera.main;
        Vector3 camWorldPosition = cam.ViewportToWorldPoint(new Vector2(0, 1));
        float brickWidth = (camScaler.cameraWidth - margin * 2) / (columns - 1) - padding * 2;
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < columns; col++)
            {
                GameObject newBrick = Instantiate(brickPrefab, new Vector3(
                    -camScaler.cameraWidth/2 + margin + (brickWidth + padding * 2) * col, 
                    camWorldPosition.y - topOffset - (brickHeight + padding * 2) * row, 0)
                    , Quaternion.identity);

                newBrick.GetComponent<Brick>().row = row;
                newBrick.GetComponent<Brick>().width = brickWidth / newBrick.transform.localScale.x;
                NetworkServer.Spawn(newBrick);
            }
        }
    }

    public void AddPoints(float points)
    {
        CmdAddPoints(points);
    }

    [Command(requiresAuthority = false)]
    private void CmdAddPoints(float points)
    {
        gameScore += points;
    }

    private void SetScore(float newScore)
    {
        SetScore(newScore, newScore);
    }

    private void SetScore(float oldScore, float newScore)
    {
        UIManager.Instance.UpdateScore((int)newScore);
    }

    public void TriggerLightFlash()
    {
        FlashLights.Invoke();
    }

    public void PlayerLost()
    {
        AudioHelpler.PlayAudio(loseSound, gameObject, Random.Range(0.5f, 0.7f), Random.Range(0.9f, 1.1f));
    }
}
