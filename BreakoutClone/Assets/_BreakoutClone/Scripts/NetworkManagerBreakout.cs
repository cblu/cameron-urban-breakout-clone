using Mirror;
using UnityEngine;

[AddComponentMenu("")]
public class NetworkManagerBreakout : NetworkManager
{
    public Transform p1Spawn;
    public Transform p2Spawn;

    private int playerCount = 0;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // Add player at correct spawn position
        Transform start = numPlayers == 0 ? p1Spawn : p2Spawn;
        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);

        player.GetComponent<Player>().playerId = playerCount;
        playerCount++;
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        // Call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);

        playerCount--;
    }
}
