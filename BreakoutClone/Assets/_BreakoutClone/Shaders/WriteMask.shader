// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// "Invisible" Occlusion Shader. Useful for AR, Masking, etc
// Mark Johns / Doomlaser - https://twitter.com/Doomlaser

Shader "Custom/WriteMask"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Color("Color",  Color) = (1, 1, 1, 1)
        _Cutoff("Alpha cutoff", Range(0,1)) = 0.5
        _StencilValue("StencilValue", Int) = 1

        [Header(DepthTest)]
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("ZTest", Float) = 4 //"LessEqual"
    }
    CGINCLUDE

    #include "UnityCG.cginc"

    struct v2f
    {
        float4 position : SV_POSITION;
        float2 texcoord : TEXCOORD0;
    };

    sampler2D _MainTex;
    float4 _MainTex_ST;
    float4 _Color;
    float _Cutoff;

    // Do a pass with depth test to write to stencil
    v2f vert(appdata_base v)
    {
        v2f o;
        o.position = UnityObjectToClipPos(v.vertex);

        o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
        return o;
    }

    float4 frag(v2f i) : COLOR
    {
        float4 c = tex2D(_MainTex, i.texcoord);
        clip(c.a - _Cutoff);
        c.a = 0;
        return c * _Color * 0;
    }

    ENDCG

    SubShader
    {
        Tags{ "RenderType" = "Transparent" "Queue" = "Geometry-1" }
        // Do a pass with depth test to write to stencil
        Stencil
        {
            Ref [_StencilValue]
            WriteMask 1
            Comp Always
            Pass Replace
            ZFail Keep
        }
        ZWrite Off
        ZTest[_ZTest]
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}