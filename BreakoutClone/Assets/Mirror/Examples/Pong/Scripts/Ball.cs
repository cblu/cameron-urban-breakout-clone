using UnityEngine;

namespace Mirror.Examples.Pong
{
    public class Ball : NetworkBehaviour
    {
        public float speed = 30;
        public Rigidbody2D rigidbody2d;

        public override void OnStartServer()
        {
            base.OnStartServer();

            // Only simulate ball physics on server
            rigidbody2d.simulated = true;
        }

        float HitFactor(Vector2 ballPos, Vector2 racketPos, float racketWidth)
        {
            // ||  1 <- at the left of the racket
            // ||
            // ||  0 <- at the middle of the racket
            // ||
            // || -1 <- at the right of the racket
            return (ballPos.x - racketPos.x) / racketWidth;
        }

        // only call this on server
        [ServerCallback]
        void OnCollisionEnter2D(Collision2D col)
        {
            // Did we hit a racket? then we need to calculate the hit factor
            if (col.transform.GetComponent<Player>())
            {
                // Calculate x direction via hit Factor
                float x = HitFactor(transform.position,
                                    col.transform.position,
                                    col.collider.bounds.size.x);

                // Calculate y direction via opposite collision
                float y = col.relativeVelocity.y > 0 ? 1 : -1;

                // Calculate direction, make length=1 via .normalized
                Vector2 dir = new Vector2(x, y).normalized;

                rigidbody2d.velocity = dir * speed;
            }
        }
    }
}
